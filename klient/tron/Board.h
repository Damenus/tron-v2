#ifndef Board_h
#define Board_h
#define N 120 //wiersze
#define M 160 //kolumny
#include<SDL.h>
#include"Player.h"

class Board {
private: 
	
	unsigned const int width = M;
	unsigned const int height = N;
public:
	
	Board();
	int get(int x, int y);
	bool set(int x, int y, int player);
	void clear();

	//TABLICA JEST TU
	int field[N][M]; //moglaby sie nazywac w sumie board, ale co wazniejsze
	int ** getField();// to chyba lepiej ja alokowac w konstruktorze
						//bo inaczej chyab sie nie da jej getowa� wyskakuje, �e typy nie pasuja
						//chyba, ze sie myle

	unsigned int getWidth();
	unsigned int getHeight();

	void renderTrails(Player * p, SDL_Renderer* winRenderer);	//renderowanie sladow
};



#endif 
