#include "Board.h"


Board::Board()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			field[i][j] = 0;
		}
	}
}
int Board::get(int x, int y)// x - wsp�rz�dna w poziomie, y - wsp�rz�dna w pionie 
{
	if (x >= 0 && x < M && y >= 0 && y < N)
	{
		return field[y][x];
	}
	return -1;
}
bool Board::set(int x, int y, int player)
{
	if (player > 0 && player < 5 && field[y][x] == 0 && x< M && y<N && x > -1)
	{
		field[y][x] = player;
		return true;
	}
	else 
		return false;
}
void Board::clear()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			field[i][j] = 0;
		}
	}
}

//int** Board::getField()
//{
//	return *field[];
//}


unsigned int Board::getWidth()
{
	return width;
}

unsigned int Board::getHeight()
{
	return height;
}

//w sumie trzeba bedzie miec w przyszlosci albo tablice obiektow wszystkich graczy albo nei wiem
//bo zeby wyswietlic gracze, musi byc tez jego obiekt ktory zawiera tekstury
//ew np flaga, czy gracz wgl bieze udzial w grze
void Board::renderTrails(Player * p, SDL_Renderer* winRenderer)
{
	for (int i = 0; i < 120; i++)
	{
		for (int j = 0; j < 160; j++)
		{
			//jezeli pole rowna sie id gracza
			if (field[i][j] == p->getId())
			{
				//pobieramy wymiary
				int w = p->getTrailTexture()->getWidth();
				int h = p->getTrailTexture()->getHeight();
				//tworzymy prostakat na podstawie ktorego bedziemy renderowac
				SDL_Rect rct = { j*w, i*h, w, h };
				//
				p->renderTrail(&rct, winRenderer);
			}

			//tu reszta dla 3 pozostalcyh powinna sie znalexc
		}
	}

}